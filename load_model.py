from keras.models import model_from_json
from wine_LSTM import sentences_to_indices, prepare_wine_data
from Base.utils_LSTM import read_glove_vecs, convert_to_one_hot
import pandas as pd
import numpy as np

# load json and create model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

df0 = pd.read_csv('Data/winemag-data-130k-v2.csv', index_col=0)

df = df0.sample(frac=0.005)
df.dropna(subset=['price'], inplace=True)
print(len(df))

X_train, Y_train, X_test, Y_test = prepare_wine_data(df)

# Convert to one hot vector
Y_oh_train = convert_to_one_hot(Y_train, C=3)
Y_oh_test = convert_to_one_hot(Y_test, C=3)

## Load glove data
word_to_index, index_to_word, word_to_vec_map = read_glove_vecs('Data/glove.6B.50d.txt')

X_test_indices = sentences_to_indices(X_test, word_to_index, max_len=maxLen)
Y_test_oh = convert_to_one_hot(Y_test, C=3)
loss, acc = loaded_model.evaluate(X_test_indices, Y_test_oh, verbose=0)
print()
print("Loss: {}, Test accuracy: {}", loss, acc)