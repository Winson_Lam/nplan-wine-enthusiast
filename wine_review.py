import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator

df = pd.read_csv('Data/winemag-data-130k-v2.csv', index_col=0)

# Exploration 1: is there is correlation between the number of points which the WineEhusiast rate the wine to the price?

def pltPriceVsPoints(loc):
    ax = plt.subplot(loc)
    X = [df.loc[df['points'] == p, 'price'].dropna().values for p in set(df['points'])]
    ax.boxplot(X, vert=True)
    ax.set_yscale('log')
    ax.set_xticklabels(set(df['points']))
    plt.ylabel('Price')
    plt.xlabel('Points')
    plt.title('Price vs Reviewer Points')

def pltPointHist(loc):
    ax = plt.subplot(loc)

    binwidth = 1
    bins=range(int(min(df['points']))-binwidth, int(max(df['points']))+binwidth, binwidth)
    ax.hist(df['points'], bins = bins)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    plt.xlabel('Points')
    plt.ylabel('Count')
    plt.xlim([min(df['points']), max(df['points'])])
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.title('Points Distribution')

# Exploration 2: what is the relationship between the types of grapes used ('variety') to the country
def plt_wineVariety(loc):
    ax = plt.subplot(loc)
    count = df.groupby('variety').size().nlargest(10)
    count_pct = 100*(count/len(df))
    x = count.index
    y = count.values
    plt.xlabel('Wine coverage (%)')
    plt.title('Wine Variety')
    plt.gca().invert_yaxis()
    ax.barh(y = x, width = count_pct)

def plt_medPriceVsProportion(loc):
    ax = plt.subplot(loc)
    x = 100*(df.groupby('variety').size()/len(df))
    y = df.groupby('variety')['price'].median()
    plt.scatter(x.values, y.values)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim([0.0005, 11])
    plt.xlabel('Wine coverage (%)')
    plt.ylabel('Price')
    plt.title('Rarity vs Price')

def plt_topTasters(loc):
    ax = plt.subplot(loc)
    n_top = 5
    freq_tasters = df['taster_name'].value_counts()[:n_top].index
    X = [df.loc[df['taster_name'] == taster, 'points'].values for taster in freq_tasters]
    ax.boxplot(X, vert=False)
    ax.set_yticklabels(freq_tasters)
    plt.xlabel('Points')
    plt.title('Wine Tasters')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))

def plt_wordDis(loc):
    word_count = df['description'].apply(lambda x: len(x.split()))
    ax = plt.subplot(loc)

    binwidth = 5
    bins = range(int(min(word_count)) - binwidth, int(max(word_count)) + binwidth, binwidth)

    ax.hist(word_count, bins = bins)
    plt.xlabel('word count')
    plt.title('Word Distribution')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    plt.ylabel('Count')

plt.figure(None, figsize=(6, 5))
pltPriceVsPoints(loc = 211)
pltPointHist(loc = 212)
plt.tight_layout(h_pad = 2, w_pad=5)
plt.savefig('Outputs/Prices_Correlation.png')
plt.show()

plt.figure(None, figsize=(12, 8))
plt_wineVariety(221)
plt_medPriceVsProportion(222)
plt_topTasters(223)
plt_wordDis(224)
plt.tight_layout(h_pad = 5, w_pad=5)
plt.savefig('Outputs/WineVariety_Proportions.png')
plt.show()

# # Exploration 3: what is the distribution of points between each taster? Is there a bias?
