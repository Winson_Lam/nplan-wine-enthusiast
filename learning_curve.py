from matplotlib import pyplot as plt

m = [298, 619, 1028, 6052, 13000]
train_error = [1.0615, 0.9966, 1.025, 1.0309, 1.0399]
test_error = [1.027, 1.139, 1.067, 1.0713, 1.0645]

plt.plot(m, train_error, label = 'training loss')
plt.plot(m, test_error, label = 'test loss')
plt.xscale('log')
plt.title('Learning Curve')
plt.xlabel('m training examples (log)')
plt.ylabel('Loss')
plt.legend()
plt.show()