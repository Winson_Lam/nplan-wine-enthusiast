import numpy as np
np.random.seed(0)
from keras.models import Model
from keras.layers import Dense, Input, Dropout, LSTM, Activation
from keras.layers.embeddings import Embedding
from Base.utils_LSTM import read_glove_vecs, convert_to_one_hot
np.random.seed(1)
import pandas as pd
import re
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, confusion_matrix, classification_report

def sentences_to_indices(X, word_to_index, max_len):
    """
    Converts an array of sentences (strings) into an array of indices corresponding to words in the sentences.

    Arguments:
    X -- array of sentences (strings), of shape (m, 1)
    word_to_index -- a dictionary containing the each word mapped to its index
    max_len -- maximum number of words in a sentence. You can assume every sentence in X is no longer than this.

    Returns:
    X_indices -- array of indices corresponding to words in the sentences from X, of shape (m, max_len)
    """

    m = X.shape[0]  # number of training examples

    # Initialize X_indices as a numpy matrix of zeros and the correct shape (≈ 1 line)
    X_indices = np.zeros(shape=(m, max_len))

    for i in range(m):  # loop over training examples

        # Convert the ith training sentence in lower case and split is into words. You should get a list of words.
        sentence_words = X[i].lower().split()

        # Initialize j to 0
        j = 0

        # Loop over the words of sentence_words
        for w in sentence_words:
        # Set the (i,j)th entry of X_indices to the index of the correct word.
            try:
                X_indices[i, j] = word_to_index[w]
                # Increment j to j + 1
                j = j + 1
            except KeyError:
                pass
    return X_indices

def pretrained_embedding_layer(word_to_vec_map, word_to_index):
    """
    Creates a Keras Embedding() layer and loads in pre-trained GloVe 50-dimensional vectors.

    Arguments:
    word_to_vec_map -- dictionary mapping words to their GloVe vector representation.
    word_to_index -- dictionary mapping from words to their indices in the vocabulary (400,001 words)

    Returns:
    embedding_layer -- pretrained layer Keras instance
    """

    vocab_len = len(word_to_index) + 1  # adding 1 to fit Keras embedding (requirement)
    emb_dim = word_to_vec_map["cucumber"].shape[0]  # define dimensionality of your GloVe word vectors (= 50), cucumber = random

    # Initialize the embedding matrix as a numpy array of zeros of shape (vocab_len, dimensions of word vectors = emb_dim)
    emb_matrix = np.zeros(shape=(vocab_len, emb_dim))

    # Set each row "index" of the embedding matrix to be the word vector representation of the "index"th word of the vocabulary
    for word, index in word_to_index.items():
        emb_matrix[index, :] = word_to_vec_map[word]

    # Define Keras embedding layer with the correct output/input sizes, make it non-trainable. Use Embedding(...). Make sure to set trainable=False.
    embedding_layer = Embedding(input_dim=vocab_len, output_dim=emb_dim, trainable=False)

    # Build the embedding layer, it is required before setting the weights of the embedding layer. Do not modify the "None".
    embedding_layer.build((None,))

    # Set the weights of the embedding layer to the embedding matrix. Your layer is now pretrained.
    embedding_layer.set_weights([emb_matrix])

    return embedding_layer


def build_model(input_shape, word_to_vec_map, word_to_index):
    """
    Function creating the model's graph.

    Arguments:
    input_shape -- shape of the input, usually (max_len,)
    word_to_vec_map -- dictionary mapping every word in a vocabulary into its 50-dimensional vector representation
    word_to_index -- dictionary mapping from words to their indices in the vocabulary (400,001 words)

    Returns:
    model -- a model instance in Keras
    """
    # Define sentence_indices as the input of the graph, it should be of shape input_shape and dtype 'int32' (as it contains indices).
    sentence_indices = Input(shape=input_shape, dtype='int32')

    # Create the embedding layer pretrained with GloVe Vectors (≈1 line)
    embedding_layer = pretrained_embedding_layer(word_to_vec_map, word_to_index)

    # Propagate sentence_indices through your embedding layer, you get back the embeddings
    embeddings = embedding_layer(sentence_indices)

    # Propagate the embeddings through an LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a batch of sequences.
    X = LSTM(128, return_sequences=True)(embeddings)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X trough another LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a single hidden state, not a batch of sequences.
    X = LSTM(128, return_sequences=False, return_state=False)(X)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X through a Dense layer with softmax activation to get back a batch of 5-dimensional vectors.
    X = Dense(3)(X)
    # Add a softmax activation
    X = Activation('softmax')(X)

    # Create Model instance which converts sentence_indices into X.
    model = Model(inputs=[sentence_indices], outputs=[X])

    return model


def prepare_wine_data(df):
    # Discretize variable into equal-sized buckets based on rank or based on sample quantiles
    df['price_class'] = pd.qcut(df['price'], 3, labels=[0, 1, 2]).astype('int')
    class_map = {0: 'Cheap', '1': 'Normal', 2: 'Expensive'}
    df['description'] = df['description'].apply(lambda x: re.sub(r'[\W+]', ' ', x.lower())).values

    train, test = train_test_split(df, test_size=0.2)
    X_train, Y_train = train['description'].values, train['price_class'].values
    X_test, Y_test = test['description'].values, test['price_class'].values
    return  X_train, Y_train, X_test, Y_test

if __name__ == '__main__':

    # Data Downloaded from https://www.kaggle.com/zynicide/wine-reviews
    df0 = pd.read_csv('Data/winemag-data-130k-v2.csv', index_col=0)

    df = df0.sample(frac=0.1)
    df.dropna(subset=['price'], inplace=True)
    print (len(df))

    X_train, Y_train, X_test, Y_test = prepare_wine_data(df)

    # Convert to one hot vector
    Y_oh_train = convert_to_one_hot(Y_train, C = 3)
    Y_oh_test = convert_to_one_hot(Y_test, C = 3)

    ## Load glove data
    # Data downloaded from https://www.kaggle.com/watts2/glove6b50dtxt/data#
    word_to_index, index_to_word, word_to_vec_map = read_glove_vecs('Data/glove.6B.50d.txt')

    maxLen = len(max(X_train, key=len).split())
    model = build_model((maxLen,), word_to_vec_map, word_to_index)
    model.summary()
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    X_train_indices = sentences_to_indices(X_train, word_to_index, maxLen)
    Y_train_oh = convert_to_one_hot(Y_train, C = 3)

    model.fit(X_train_indices, Y_train_oh, epochs = 50, batch_size = 32, shuffle=True)

    X_test_indices = sentences_to_indices(X_test, word_to_index, max_len = maxLen)
    Y_test_oh = convert_to_one_hot(Y_test, C = 3)
    loss, acc = model.evaluate(X_test_indices, Y_test_oh, verbose=0)
    print()
    print("Loss: {}, Test accuracy: {}", loss, acc)


    y_pred_keras = model.predict(X_test_indices).argmax(axis = 1)

    print (confusion_matrix(Y_test_oh.argmax(axis=1), y_pred_keras))
    print ()
    print (classification_report(Y_test_oh.argmax(axis=1), y_pred_keras, digits=3))
    print()



